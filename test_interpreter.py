import unittest
import MathInterpreter.analysis as _ali
import MathInterpreter.data_structures.nodes as _nds


class TestInterpreter(unittest.TestCase):
    def test_numbers(self):
        value = _ali.Interpreter().visit(_nds.NumberNode(51.2))
        self.assertEqual(value, _nds.NumberNode(51.2))

        value = _ali.Interpreter().visit(_nds.NumberNode(0.500))
        self.assertEqual(value, _nds.NumberNode(0.5))

    def test_constants(self):
        value = _ali.Interpreter().visit(_nds.PiNode())
        self.assertEqual(value, _nds.NumberNode(3.14159))

        value = _ali.Interpreter().visit(_nds.EulersNode())
        self.assertEqual(value, _nds.NumberNode(2.71828))

    def test_single_operations_add(self):
        result = _ali.Interpreter().visit(_nds.AddNode(_nds.NumberNode(3), _nds.NumberNode(0.5)))
        self.assertEqual(result.value, 3.5)

        result = _ali.Interpreter().visit(_nds.AddNode(_nds.NumberNode(0.25), _nds.NumberNode(0.75)))
        self.assertEqual(result.value, 1)

        result = _ali.Interpreter().visit(_nds.AddNode(_nds.NumberNode(0.2222), _nds.NumberNode(0.1111)))
        self.assertAlmostEqual(result.value, 0.3333, 4)

        result = _ali.Interpreter().visit(_nds.AddNode(_nds.NumberNode(72), _nds.NumberNode(9)))
        self.assertEqual(result.value, 81)

    def test_single_operations_subtract(self):
        result = _ali.Interpreter().visit(_nds.SubtractNode(_nds.NumberNode(3), _nds.NumberNode(0.5)))
        self.assertEqual(result.value, 2.5)

        result = _ali.Interpreter().visit(_nds.SubtractNode(_nds.NumberNode(0.75), _nds.NumberNode(0.25)))
        self.assertEqual(result.value, 0.5)

        result = _ali.Interpreter().visit(_nds.SubtractNode(_nds.NumberNode(0.3333), _nds.NumberNode(0.1111)))
        self.assertAlmostEqual(result.value, 0.2222, 4)

        result = _ali.Interpreter().visit(_nds.SubtractNode(_nds.NumberNode(81), _nds.NumberNode(9)))
        self.assertEqual(result.value, 72)

    def test_single_operations_multiply(self):
        result = _ali.Interpreter().visit(_nds.MultiplyNode(_nds.NumberNode(3), _nds.NumberNode(0.5)))
        self.assertEqual(result.value, 1.5)

        result = _ali.Interpreter().visit(_nds.MultiplyNode(_nds.NumberNode(0.5), _nds.NumberNode(0.5)))
        self.assertEqual(result.value, 0.25)

        result = _ali.Interpreter().visit(_nds.MultiplyNode(_nds.NumberNode(0.1), _nds.NumberNode(0.5)))
        self.assertEqual(result.value, 0.05)

        result = _ali.Interpreter().visit(_nds.MultiplyNode(_nds.NumberNode(9), _nds.NumberNode(9)))
        self.assertEqual(result.value, 81)

    def test_single_operations_divide(self):
        result = _ali.Interpreter().visit(_nds.DivideNode(_nds.NumberNode(3.5), _nds.NumberNode(4)))
        self.assertEqual(result.value, 0.875)

        result = _ali.Interpreter().visit(_nds.DivideNode(_nds.NumberNode(0.5), _nds.NumberNode(0.5)))
        self.assertEqual(result.value, 1)

        result = _ali.Interpreter().visit(_nds.DivideNode(_nds.NumberNode(0.1), _nds.NumberNode(0.5)))
        self.assertEqual(result.value, 0.2)

        result = _ali.Interpreter().visit(_nds.DivideNode(_nds.NumberNode(81), _nds.NumberNode(9)))
        self.assertEqual(result.value, 9)

        result = _ali.Interpreter().visit(_nds.DivideNode(_nds.NumberNode(27), _nds.NumberNode(14)))
        self.assertAlmostEqual(result.value, 1.92857, 5)

    def test_single_operations_squareroot(self):
        result = _ali.Interpreter().visit(_nds.SqRootNode(_nds.NumberNode(9)))
        self.assertEqual(result.value, 3)

        result = _ali.Interpreter().visit(_nds.SqRootNode(_nds.NumberNode(27), _nds.NumberNode(3)))
        self.assertEqual(result.value, 3)

        result = _ali.Interpreter().visit(_nds.SqRootNode(_nds.NumberNode(20.25), _nds.NumberNode(2)))
        self.assertEqual(result.value, 4.5)

        result = _ali.Interpreter().visit(_nds.SqRootNode(_nds.NumberNode(274.625), _nds.NumberNode(3)))
        self.assertAlmostEqual(result.value, 6.5, 1)

        result = _ali.Interpreter().visit(_nds.AddNode(_nds.SqRootNode(_nds.NumberNode(9), _nds.NumberNode(2)), _nds.NumberNode(3)))
        self.assertEqual(result.value, 6)

    def test_single_operations_percent(self):
        result = _ali.Interpreter().visit(_nds.PercentNode(_nds.AddNode(_nds.NumberNode(200), _nds.NumberNode(10))))
        self.assertEqual(result.value, 220)

        result = _ali.Interpreter().visit(_nds.PercentNode(_nds.SubtractNode(_nds.NumberNode(200), _nds.NumberNode(10))))
        self.assertEqual(result.value, 180)

        result = _ali.Interpreter().visit(_nds.PercentNode(_nds.MultiplyNode(_nds.NumberNode(200), _nds.NumberNode(10))))
        self.assertEqual(result.value, 20)

        result = _ali.Interpreter().visit(_nds.PercentNode(_nds.DivideNode(_nds.NumberNode(200), _nds.NumberNode(10))))
        self.assertEqual(result.value, 2000)

    def test_operations_square(self):
        result = _ali.Interpreter().visit(_nds.SquareNode(_nds.NumberNode(3), _nds.NumberNode(2)))
        self.assertEqual(result.value, 9)

        result = _ali.Interpreter().visit(_nds.SquareNode(_nds.NumberNode(3), _nds.NumberNode(3)))
        self.assertEqual(result.value, 27)

        result = _ali.Interpreter().visit(_nds.SquareNode(_nds.NumberNode(4), _nds.NumberNode(4)))
        self.assertEqual(result.value, 256)

        result = _ali.Interpreter().visit(_nds.SquareNode(_nds.NumberNode(3), _nds.SquareNode(_nds.NumberNode(2), _nds.NumberNode(2))))
        self.assertEqual(result.value, 81)

    def test_operations_fail(self):
        with self.assertRaises(ZeroDivisionError):
            _ali.Interpreter().visit(_nds.DivideNode(_nds.NumberNode(27), _nds.NumberNode(0)))

    def test_full_expression(self):
        result = _ali.Interpreter().visit(
            _nds.AddNode(
                _nds.NumberNode(27),
                _nds.MultiplyNode(
                    _nds.SubtractNode(_nds.DivideNode(_nds.NumberNode(43), _nds.NumberNode(36)), _nds.NumberNode(48)), _nds.NumberNode(51)
                ),
            )
        )
        self.assertAlmostEqual(result.value, -2360.08, 2)
