#! /usr/bin/env python3
# coding: utf8
from MathInterpreter.tools import *


def main():
    api = API(debug_mode=False)
    while True:
        try:
            str_input = input("calc > ")
            print(api.get_result_by_str(str_input))
        except Exception as e:
            print(e)


if __name__ == "__main__":
    main()
