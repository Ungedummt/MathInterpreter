import unittest
from MathInterpreter.tools import API


class TestMain(unittest.TestCase):
    def test_all(self):
        api = API()

        value = api.get_result_by_str("(2+2*9/4-12*(2+6)^3)*50%")
        self.assertEqual(value, -3068.75)

        value = api.get_result_by_str("(2+2*9/4-12*(2+6)^3)*.5")
        self.assertEqual(value, -3068.75)

        value = api.get_result_by_str("2^3+4*5/20-3")
        self.assertEqual(value, 6)
