#!/bin/bash

printf "Run: lexer test\n\n"
/usr/bin/python3 -m unittest test_lexer
printf "\n\n\nRun: parser test\n\n"
/usr/bin/python3 -m unittest test_parser
printf "\n\n\nRun: interpreter test\n\n"
/usr/bin/python3 -m unittest test_interpreter
printf "\n\n\nRun: main test\n\n"
/usr/bin/python3 -m unittest test_main
