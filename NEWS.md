## v0.1: (Changes since the fork form David P. Callanans project)

 - Added pi and e as numbers
 - Added operations:
   - square
   - square-root
   - percent
 - Added CI pipelines
 - Code structure refactoring
 - Added somewhat of an "API"
 - Extended tests
 - Added bash file for running all tests
