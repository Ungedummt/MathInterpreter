import unittest
import MathInterpreter.analysis as _ali
import MathInterpreter.data_structures.tokens as _tks


class TestLexer(unittest.TestCase):
    def test_empty(self):
        tokens = list(_ali.Lexer("").generate_tokens())
        self.assertEqual(tokens, [])

    def test_whitespace(self):
        tokens = list(_ali.Lexer(" \t\n  \t\t\n\n").generate_tokens())
        self.assertEqual(tokens, [])

    def test_numbers(self):
        tokens = list(_ali.Lexer("123 123.456 123. .456 . ¹³²").generate_tokens())
        self.assertEqual(
            tokens,
            [
                _tks.Token(_tks.TokenType.NUMBER, 123.000),
                _tks.Token(_tks.TokenType.NUMBER, 123.456),
                _tks.Token(_tks.TokenType.NUMBER, 123.000),
                _tks.Token(_tks.TokenType.NUMBER, 000.456),
                _tks.Token(_tks.TokenType.NUMBER, 000.000),
                _tks.Token(_tks.TokenType.SQUARE),
                _tks.Token(_tks.TokenType.NUMBER, 132.000),
            ],
        )

    def test_constants(self):
        tokens = list(_ali.Lexer("pi e").generate_tokens())
        self.assertEqual(
            tokens,
            [
                _tks.Token(_tks.TokenType.PI),
                _tks.Token(_tks.TokenType.EULERS),
            ]
        )

    def test_operators(self):
        tokens = list(_ali.Lexer("+-*/^%sqrt").generate_tokens())
        self.assertEqual(
            tokens,
            [
                _tks.Token(_tks.TokenType.PLUS),
                _tks.Token(_tks.TokenType.MINUS),
                _tks.Token(_tks.TokenType.MULTIPLY),
                _tks.Token(_tks.TokenType.DIVIDE),
                _tks.Token(_tks.TokenType.SQUARE),
                _tks.Token(_tks.TokenType.PERCENT),
                _tks.Token(_tks.TokenType.SQROOT),
            ],
        )

    def test_special_characters(self):
        tokens = list(_ali.Lexer("(,)").generate_tokens())
        self.assertEqual(
            tokens,
            [
                _tks.Token(_tks.TokenType.LPAREN),
                _tks.Token(_tks.TokenType.COMMA),
                _tks.Token(_tks.TokenType.RPAREN),
            ],
        )

    def test_all(self):
        tokens = list(_ali.Lexer("sqrt3(64) + sqrt(9,2) + sqrt(81)").generate_tokens())
        self.assertEqual(
            tokens,
            [
                _tks.Token(_tks.TokenType.SQROOT),
                _tks.Token(_tks.TokenType.NUMBER, 3),
                _tks.Token(_tks.TokenType.LPAREN),
                _tks.Token(_tks.TokenType.NUMBER, 64),
                _tks.Token(_tks.TokenType.RPAREN),
                _tks.Token(_tks.TokenType.PLUS),
                _tks.Token(_tks.TokenType.SQROOT),
                _tks.Token(_tks.TokenType.LPAREN),
                _tks.Token(_tks.TokenType.NUMBER, 9),
                _tks.Token(_tks.TokenType.COMMA),
                _tks.Token(_tks.TokenType.NUMBER, 2),
                _tks.Token(_tks.TokenType.RPAREN),
                _tks.Token(_tks.TokenType.PLUS),
                _tks.Token(_tks.TokenType.SQROOT),
                _tks.Token(_tks.TokenType.LPAREN),
                _tks.Token(_tks.TokenType.NUMBER, 81),
                _tks.Token(_tks.TokenType.RPAREN),
            ],
        )

        tokens = list(_ali.Lexer("27 + (43 / 36 - 48 ^3) * 51⁷ + (200 + 10%)").generate_tokens())
        self.assertEqual(
            tokens,
            [
                _tks.Token(_tks.TokenType.NUMBER, 27),
                _tks.Token(_tks.TokenType.PLUS),
                _tks.Token(_tks.TokenType.LPAREN),
                _tks.Token(_tks.TokenType.NUMBER, 43),
                _tks.Token(_tks.TokenType.DIVIDE),
                _tks.Token(_tks.TokenType.NUMBER, 36),
                _tks.Token(_tks.TokenType.MINUS),
                _tks.Token(_tks.TokenType.NUMBER, 48),
                _tks.Token(_tks.TokenType.SQUARE),
                _tks.Token(_tks.TokenType.NUMBER, 3),
                _tks.Token(_tks.TokenType.RPAREN),
                _tks.Token(_tks.TokenType.MULTIPLY),
                _tks.Token(_tks.TokenType.NUMBER, 51),
                _tks.Token(_tks.TokenType.SQUARE),
                _tks.Token(_tks.TokenType.NUMBER, 7),
                _tks.Token(_tks.TokenType.PLUS),
                _tks.Token(_tks.TokenType.LPAREN),
                _tks.Token(_tks.TokenType.NUMBER, 200),
                _tks.Token(_tks.TokenType.PLUS),
                _tks.Token(_tks.TokenType.NUMBER, 10),
                _tks.Token(_tks.TokenType.PERCENT),
                _tks.Token(_tks.TokenType.RPAREN),
            ],
        )
