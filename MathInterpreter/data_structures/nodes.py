from dataclasses import dataclass


@dataclass
class NumberNode:
    value: any

    def __repr__(self):
        return f"{self.value}"


@dataclass
class AddNode:
    node_a: any
    node_b: any

    def __repr__(self):
        return f"({self.node_a}+{self.node_b})"


@dataclass
class SubtractNode:
    node_a: any
    node_b: any

    def __repr__(self):
        return f"({self.node_a}-{self.node_b})"


@dataclass
class MultiplyNode:
    node_a: any
    node_b: any

    def __repr__(self):
        return f"({self.node_a}*{self.node_b})"


@dataclass
class DivideNode:
    node_a: any
    node_b: any

    def __repr__(self):
        return f"({self.node_a}/{self.node_b})"


@dataclass
class PlusNode:
    node: any

    def __repr__(self):
        return f"+({self.node})"


@dataclass
class MinusNode:
    node: any

    def __repr__(self):
        return f"-({self.node})"


@dataclass
class SquareNode:
    node_a: any
    node_b: any

    def __repr__(self):
        return f"({self.node_a}^{self.node_b})"


@dataclass
class ParenNode:
    value_a: any
    value_b: any

    def __repr__(self):
        return f"({self.node_a}, {self.node_b})"


@dataclass
class PercentNode:
    node: any

    def __repr__(self):
        return f"({self.node})%"


@dataclass
class StringNode:
    value: any

    def __repr__(self):
        return f"{self.value}"


@dataclass
class SqRootNode:
    node: any
    value: any = NumberNode(2)

    def __repr__(self):
        return f"sqrt({self.node}, {self.value})"


@dataclass
class PiNode:
    def __repr__(self):
        return f"pi"


@dataclass
class EulersNode:
    def __repr__(self):
        return f"e"
