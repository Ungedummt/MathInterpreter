from dataclasses import dataclass


@dataclass
class Number:
    """
    This no longer used but it's needed eventually in the future.
    """

    value: any

    def __repr__(self):
        return f"{self.value}"
