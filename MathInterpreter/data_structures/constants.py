from dataclasses import dataclass

@dataclass
class Constants:
    PI = 3.14159
    EULER = 2.71828
