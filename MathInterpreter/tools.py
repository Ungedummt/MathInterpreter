from string import digits, ascii_letters
import MathInterpreter.analysis as _ali


class API:
    """
    (API Version: 0.1)
    With this I try to create a stable API that can be used in programs
    """

    def __init__(self, debug_mode=False):
        self.debug_mode = debug_mode
        self.print_debug = Helpers(debug_mode=debug_mode).print_debug

    def get_result_by_str(self, str_input: str) -> float:
        if str_input is not None:
            self.print_debug(f"Input:  '{str_input}'")
            helpers = Helpers(debug_mode=self.debug_mode)
            str_input = helpers.pre_format_str(str_input)
            # helpers.check_str(str_input)
            tokens = _ali.Lexer(str_input, debug_mode=self.debug_mode).generate_tokens()
            tree = _ali.Parser(tokens, debug_mode=self.debug_mode).parse()
            self.print_debug(f"Tree:   '{tree}'")
            return _ali.Interpreter(debug_mode=self.debug_mode).visit(tree).value
        else:
            raise TypeError("Please add a value that's not None")

    def get_tokens_by_str(self, str_input: str):
        if str_input is not None:
            self.print_debug(f"Input:  '{str_input}'")
            helpers = Helpers(debug_mode=self.debug_mode)
            str_input = helpers.pre_format_str(str_input)
            tokens = _ali.Lexer(str_input, debug_mode=self.debug_mode).generate_tokens()
            token_list = []
            current_token = next(iter(tokens))
            while current_token != None:
                token_list.append(current_token)
                try:
                    current_token = next(tokens)
                except StopIteration:
                    current_token = None

            return token_list
        else:
            raise TypeError("Please add a value that's not None")

    def get_tree_by_str(self, str_input: str):
        if str_input is not None:
            self.print_debug(f"Input:  '{str_input}'")
            helpers = Helpers(debug_mode=self.debug_mode)
            str_input = helpers.pre_format_str(str_input)
            tokens = _ali.Lexer(str_input, debug_mode=self.debug_mode).generate_tokens()
            tree = _ali.Parser(tokens, debug_mode=self.debug_mode).parse()
            return tree
        else:
            raise TypeError("Please add a value that's not None")

    def get_api_version(self):
        return "Version: 0.1"

    def gen_calculation(self):
        pass  # TODO generate valid calculations


class Helpers:
    """
    This are all helper functions needed for the API class. But it could also used outside of the API
    """

    supported_squared = "⁰¹²³⁴⁵⁶⁷⁸⁹"
    supported_symbols = ".,+-*/^%() \n\t" + supported_squared + digits + ascii_letters
    supported_calc_symbols = "+-*/^%" + supported_squared

    def __init__(self, debug_mode=False):
        self.debug_mode = debug_mode

    def print_debug(self, var_to_print) -> None:
        if self.debug_mode:
            print(f"Debug: {var_to_print}")
        else:
            pass

    def check_str(self, str_to_check: str):
        # This is to check if there is no unnecessary things
        str_replace = str_to_check
        str_replace = str_replace.replace("(", "").replace(")", "")
        if str_replace == None or str_replace == "":
            raise Exception("Nothing to calculate")

        for i in str_to_check:
            if not i in self.supported_symbols:
                raise Exception(f"Illegal character '{i}'")

        if str_to_check.count("(") != str_to_check.count(")"):
            raise Exception("Please proof the parentheses")

        # This checks if there is at least one calculation
        # one_calc = False
        # for i in str_to_check:
        #    if i in self.supported_calc_symbols:
        #        one_calc = True
        # if not one_calc:
        #    raise Exception("Nothing to calculate")

        if str_to_check[-1:] in "+-*/^":
            raise Exception("Invalid syntax")

    @staticmethod
    def pre_format_str(str_to_format: str):
        # Format a little bit
        str_to_format = str_to_format.replace(" ", "")
        str_to_format = str_to_format.replace("\n", "")
        str_to_format = str_to_format.replace("\t", "")

        if str_to_format == None or str_to_format == "" or str_to_format == "()":
            raise Exception("Nothing to calculate")

        str_to_format = str_to_format.lower()

        return str_to_format
