from string import digits, ascii_letters
import MathInterpreter.data_structures.nodes as _nds
import MathInterpreter.data_structures.tokens as _tks
import MathInterpreter.data_structures.constants as _cst
import MathInterpreter.tools as _tls


class Interpreter:
    def __init__(self, debug_mode=False):
        self.print_debug = _tls.Helpers(debug_mode=debug_mode).print_debug

    def visit(self, node):
        self.print_debug(f"Interpreter: visit {type(node).__name__}")
        method_name = f"visit_{type(node).__name__}"
        method = getattr(self, method_name)
        return method(node)

    def visit_NumberNode(self, node):
        return _nds.NumberNode(node.value)

    def visit_AddNode(self, node):
        return _nds.NumberNode(self.visit(node.node_a).value + self.visit(node.node_b).value)

    def visit_SubtractNode(self, node):
        return _nds.NumberNode(self.visit(node.node_a).value - self.visit(node.node_b).value)

    def visit_MultiplyNode(self, node):
        return _nds.NumberNode(self.visit(node.node_a).value * self.visit(node.node_b).value)

    def visit_DivideNode(self, node):
        try:
            return _nds.NumberNode(self.visit(node.node_a).value / self.visit(node.node_b).value)
        except ZeroDivisionError:
            raise ZeroDivisionError("you can divide by zero")

    def visit_PlusNode(self, node):
        return self.visit(node.node)

    def visit_MinusNode(self, node):
        return _nds.NumberNode(-self.visit(node.node).value)

    def visit_SquareNode(self, node):
        return _nds.NumberNode(self.visit(node.node_a).value ** self.visit(node.node_b).value)

    def visit_PercentNode(self, node):
        node = node.node
        value_a = self.visit(node.node_a).value
        value_b = self.visit(node.node_b).value
        node_name = type(node).__name__

        if node_name in ("AddNode", "SubtractNode"):
            number = value_a * (value_b / 100)
            if node_name == "AddNode":
                return _nds.NumberNode(value_a + number)
            else:
                return _nds.NumberNode(value_a - number)

        elif node_name in ("MultiplyNode", "DivideNode"):
            number = value_b / 100
            if node_name == "MultiplyNode":
                return _nds.NumberNode(value_a * number)
            else:
                return _nds.NumberNode(value_a / number)

        raise Exception(f"Can't use the {node_name} with percent")

    def visit_SqRootNode(self, node):
        return _nds.NumberNode(self.visit(node.node).value ** (1 / self.visit(node.value).value))

    def visit_PiNode(self, node):
        return _nds.NumberNode(_cst.Constants.PI)

    def visit_EulersNode(self, node):
        return _nds.NumberNode(_cst.Constants.EULER)


class Lexer:
    WHITESPACE = " \n\t"
    SQUARED = "⁰¹²³⁴⁵⁶⁷⁸⁹"

    def __init__(self, text, debug_mode=False):
        self.text = iter(text)
        self.print_debug = _tls.Helpers(debug_mode=debug_mode).print_debug
        self._advance()

    def _advance(self):
        try:
            self.current_char = next(self.text)
        except StopIteration:
            self.current_char = None

    def generate_tokens(self):
        while self.current_char != None:
            self.print_debug(f"Lexer:  current Char:  '{self.current_char}'")
            if self.current_char in self.WHITESPACE:
                self._advance()
            elif self.current_char == "." or self.current_char in digits:
                yield self._generate_number()
            elif self.current_char in self.SQUARED:
                yield _tks.Token(_tks.TokenType.SQUARE)
                yield self._generate_squared()
            elif self.current_char in ascii_letters:
                yield self._generate_string()
            elif self.current_char == "+":
                self._advance()
                yield _tks.Token(_tks.TokenType.PLUS)
            elif self.current_char == "-":
                self._advance()
                yield _tks.Token(_tks.TokenType.MINUS)
            elif self.current_char == "*":
                self._advance()
                yield _tks.Token(_tks.TokenType.MULTIPLY)
            elif self.current_char == "/":
                self._advance()
                yield _tks.Token(_tks.TokenType.DIVIDE)
            elif self.current_char == "(":
                self._advance()
                yield _tks.Token(_tks.TokenType.LPAREN)
            elif self.current_char == ")":
                self._advance()
                yield _tks.Token(_tks.TokenType.RPAREN)
            elif self.current_char == "^":
                self._advance()
                yield _tks.Token(_tks.TokenType.SQUARE)
            elif self.current_char == "%":
                self._advance()
                yield _tks.Token(_tks.TokenType.PERCENT)
            elif self.current_char == ",":
                self._advance()
                yield _tks.Token(_tks.TokenType.COMMA)
            else:
                raise Exception(f"Illegal character '{self.current_char}'")

    def _generate_string(self):
        letter_str = self.current_char
        self._advance()

        # NOTE: self.current_char != None needs to be at first to not fail
        while self.current_char != None and self.current_char in ascii_letters:
            letter_str += self.current_char
            self._advance()

        # Here can be things added like pi or other custom strings
        if letter_str == "sqrt":
            return _tks.Token(_tks.TokenType.SQROOT)
        elif letter_str == "pi":
            return _tks.Token(_tks.TokenType.PI)
        elif letter_str == "e":
            return _tks.Token(_tks.TokenType.EULERS)

        return _tks.Token(_tks.TokenType.STRING, str(letter_str))

    def _generate_number(self):
        """
        It should recognize all types of numbers. For example it should interpret ".82" as "0.82" or "82." as "82.0"
        """
        # TODO: It should also could deal with Number's like "8,282.82" but not with "8,28,2.82" such
        decimal_point_count = 0
        number_str = self.current_char
        self._advance()

        while self.current_char != None and (self.current_char == "." or self.current_char in digits):
            if self.current_char == ".":
                decimal_point_count += 1
                if decimal_point_count > 1:
                    break

            number_str += self.current_char
            self._advance()

        if number_str.startswith("."):
            number_str = "0" + number_str
        if number_str.endswith("."):
            number_str += "0"

        return _tks.Token(_tks.TokenType.NUMBER, float(number_str))

    def _generate_squared(self):
        number_str = self.current_char
        self._advance()

        while self.current_char != None and self.current_char in self.SQUARED:
            number_str += self.current_char
            self._advance()

        return _tks.Token(_tks.TokenType.NUMBER, float(self._squared_to_number(number_str)))

    def _squared_to_number(self, squared_numbers: str):
        transtab = str.maketrans(self.SQUARED, digits)
        return int(squared_numbers.translate(transtab))


class Parser:
    def __init__(self, tokens, debug_mode=False):
        self.tokens = iter(tokens)
        self.print_debug = _tls.Helpers(debug_mode=debug_mode).print_debug
        self._advance()

    def _raise_error(self):
        raise Exception("Invalid syntax")

    def _advance(self):
        try:
            self.current_token = next(self.tokens)
            self.print_debug(f"Parser: current Token: '{self.current_token}'")
        except StopIteration:
            self.current_token = None

    def parse(self):
        if self.current_token == None:
            return None

        result = self._expr()

        if self.current_token != None:
            self._raise_error()

        return result

    def _percent(self, result):
        """
        This function should be everywhere added where the percent operator offer special funtionality.
        Please keep in mind that it should also implemented in the Interpreter.
        """

        # NOTE: self.current_token != None needs to be at first to not fail
        if self.current_token != None and self.current_token.type == _tks.TokenType.PERCENT:
            self._advance()
            return _nds.PercentNode(result)

        return result

    def _expr(self):
        result = self._term()

        while self.current_token != None and self.current_token.type in (
            _tks.TokenType.PLUS,
            _tks.TokenType.MINUS,
        ):

            if self.current_token.type == _tks.TokenType.PLUS:
                self._advance()
                result = _nds.AddNode(result, self._term())
                result = self._percent(result)
            elif self.current_token.type == _tks.TokenType.MINUS:
                self._advance()
                result = _nds.SubtractNode(result, self._term())
                result = self._percent(result)

        return result

    def _term(self):
        result = self._square()

        while self.current_token != None and self.current_token.type in (
            _tks.TokenType.MULTIPLY,
            _tks.TokenType.DIVIDE,
        ):

            if self.current_token.type == _tks.TokenType.MULTIPLY:
                self._advance()
                result = _nds.MultiplyNode(result, self._square())
                result = self._percent(result)
            elif self.current_token.type == _tks.TokenType.DIVIDE:
                self._advance()
                result = _nds.DivideNode(result, self._square())
                result = self._percent(result)

        return result

    def _square(self):
        result = self._sq_root()

        if self.current_token != None and self.current_token.type == _tks.TokenType.SQUARE:
            self._advance()
            # NOTE: do not change it from self._square() to self._factor()
            # If you still don't believe it future me, change it and calculate 3^3^3 than
            # compare it with another calculator and print out the tree var in main.py (;
            result = _nds.SquareNode(result, self._square())

        return result

    def _sq_root(self):
        result = self._factor()

        token = self.current_token

        if self.current_token != None and token.type == _tks.TokenType.SQROOT:
            self._advance()
            token = self.current_token
            # case: sqrt $NUMBER ...
            if token.type == _tks.TokenType.NUMBER:
                self._advance()
                # NOTE: don't remove: 'NoneType' object has no attribute 'type'
                # case: sqrt $NUMBER
                if self.current_token == None or self.current_token.type != _tks.TokenType.LPAREN:
                    return _nds.SqRootNode(_nds.NumberNode(token.value))
                # case: sqrt $NUMBER ($NUMBER)
                elif self.current_token.type == _tks.TokenType.LPAREN:
                    return _nds.SqRootNode(self._factor(), _nds.NumberNode(token.value))
            # case: sqrt ( ... )
            elif token.type == _tks.TokenType.LPAREN:
                return self._sq_root_paren()

        return result

    def _sq_root_paren(self):
        token = self.current_token

        if token.type == _tks.TokenType.LPAREN:
            self._advance()
            node = self._expr()
            value = _nds.NumberNode(2)

            if self.current_token.type == _tks.TokenType.COMMA:
                self._advance()
                value = self._expr()

            if self.current_token.type != _tks.TokenType.RPAREN:
                self._raise_error()

            self._advance()
            return _nds.SqRootNode(node, value)

        else:
            raise Exception("Please run this func only if current_token is TokenType.LPAREN")

    def _factor(self):
        token = self.current_token

        if token.type == _tks.TokenType.LPAREN:
            self._advance()
            result = self._expr()

            if self.current_token.type != _tks.TokenType.RPAREN:
                self._raise_error()
            # else: not needed but it is like an else
            self._advance()

            return result

        elif token.type == _tks.TokenType.NUMBER:
            self._advance()
            return _nds.NumberNode(token.value)

        elif token.type == _tks.TokenType.PI:
            self._advance()
            return _nds.PiNode()

        elif token.type == _tks.TokenType.EULERS:
            self._advance()
            return _nds.EulersNode()

        elif token.type == _tks.TokenType.PLUS:
            self._advance()
            return _nds.PlusNode(self._factor())

        elif token.type == _tks.TokenType.MINUS:
            self._advance()
            return _nds.MinusNode(self._factor())
