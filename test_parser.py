import unittest
import MathInterpreter.analysis as _ali
import MathInterpreter.data_structures.nodes as _nds
import MathInterpreter.data_structures.tokens as _tks


class TestParser(unittest.TestCase):
    def test_empty(self):
        tokens = []
        node = _ali.Parser(tokens).parse()
        self.assertEqual(node, None)

    def test_numbers(self):
        tokens = [_tks.Token(_tks.TokenType.NUMBER, 51.2)]
        node = _ali.Parser(tokens).parse()
        self.assertEqual(node, _nds.NumberNode(51.2))

    def test_constants(self):
        tokens = [_tks.Token(_tks.TokenType.PI)]
        node = _ali.Parser(tokens).parse()
        self.assertEqual(node, _nds.PiNode())

        tokens = [_tks.Token(_tks.TokenType.EULERS)]
        node = _ali.Parser(tokens).parse()
        self.assertEqual(node, _nds.EulersNode())

    def test_single_operations(self):
        tokens = [
            _tks.Token(_tks.TokenType.NUMBER, 27),
            _tks.Token(_tks.TokenType.PLUS),
            _tks.Token(_tks.TokenType.NUMBER, 42),
        ]

        node = _ali.Parser(tokens).parse()
        self.assertEqual(node, _nds.AddNode(_nds.NumberNode(27), _nds.NumberNode(42)))

        tokens = [
            _tks.Token(_tks.TokenType.NUMBER, 27),
            _tks.Token(_tks.TokenType.SQUARE),
            _tks.Token(_tks.TokenType.NUMBER, 42),
        ]

        node = _ali.Parser(tokens).parse()
        self.assertEqual(node, _nds.SquareNode(_nds.NumberNode(27), _nds.NumberNode(42)))

        tokens = [
            _tks.Token(_tks.TokenType.SQROOT),
            _tks.Token(_tks.TokenType.NUMBER, 9),
        ]

        node = _ali.Parser(tokens).parse()
        self.assertEqual(node, _nds.SqRootNode(_nds.NumberNode(9), _nds.NumberNode(2)))

    def test_percent(self):
        tokens = [
            _tks.Token(_tks.TokenType.NUMBER, 200),
            _tks.Token(_tks.TokenType.DIVIDE),
            _tks.Token(_tks.TokenType.NUMBER, 25),
            _tks.Token(_tks.TokenType.PERCENT),
        ]

        node = _ali.Parser(tokens).parse()
        self.assertEqual(node, _nds.PercentNode(_nds.DivideNode(_nds.NumberNode(200), _nds.NumberNode(25))))

    def test_full_expression(self):
        tokens = [
            _tks.Token(_tks.TokenType.LPAREN),
            _tks.Token(_tks.TokenType.NUMBER, 27),
            _tks.Token(_tks.TokenType.PLUS),
            _tks.Token(_tks.TokenType.LPAREN),
            _tks.Token(_tks.TokenType.NUMBER, 43),
            _tks.Token(_tks.TokenType.DIVIDE),
            _tks.Token(_tks.TokenType.NUMBER, 36),
            _tks.Token(_tks.TokenType.MINUS),
            _tks.Token(_tks.TokenType.NUMBER, 48),
            _tks.Token(_tks.TokenType.RPAREN),
            _tks.Token(_tks.TokenType.MULTIPLY),
            _tks.Token(_tks.TokenType.NUMBER, 51),
            _tks.Token(_tks.TokenType.SQUARE),
            _tks.Token(_tks.TokenType.NUMBER, 42),
            _tks.Token(_tks.TokenType.RPAREN),
            _tks.Token(_tks.TokenType.MULTIPLY),
            _tks.Token(_tks.TokenType.NUMBER, 25),
            _tks.Token(_tks.TokenType.PERCENT),
        ]

        node = _ali.Parser(tokens).parse()
        self.assertEqual(
            node,
            _nds.PercentNode(
                _nds.MultiplyNode(
                    _nds.AddNode(
                        _nds.NumberNode(27),
                        _nds.MultiplyNode(
                            _nds.SubtractNode(_nds.DivideNode(_nds.NumberNode(43), _nds.NumberNode(36)), _nds.NumberNode(48)),
                            _nds.SquareNode(_nds.NumberNode(51), _nds.NumberNode(42)),
                        ),
                    ),
                    _nds.NumberNode(25),
                )
            ),
        )
