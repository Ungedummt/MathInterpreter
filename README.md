# Python - MathInterpreter

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![pipeline status](https://gitlab.gnome.org/Ungedummt/MathInterpreter/badges/master/pipeline.svg)](https://gitlab.gnome.org/Ungedummt/MathInterpreter/-/commits/master)

This is a fork of the py-simple-math-interpreter program from davidcallanan on GitHub. You can find it [here](https://github.com/davidcallanan/py-simple-math-interpreter).


An interpreter, written from scratch in Python, that can evaluate simple math calculations.

This is useful for learning how computers process human-readable text and is a great first step to creating your own programming language, data language, etc.

The user input is analyzed in two sections of code called the lexer and parser, before finally being interpreted by the interpreter.

## Lexer

The lexer groups the input characters into small segments called tokens and identifies the type of each token, similarly to how we group letters into words such as nouns and verbs.

The characters in the input `12 + 24` are grouped into the tokens `NUMBER:12`, `PLUS`, and `NUMBER:24`.

Whitespace is usually ignored by the lexer.

The tokens are then passed on to the parser.

## Parser

The parser analyzes the sequence of tokens to determine what is intended to happen and in what order, similarly to how we make sense of sentences based on the sequence and types of words.

When the parser sees `NUMBER`, followed by `PLUS`, followed by `NUMBER`, it passes on that the two numbers should be added together. In the case of a multiply operation added into the mix, the parser can determine that the two numbers next to the multiply operator should be multiplied first before the addition takes place.

The result, represented as a tree, is then passed on to the interpreter.

## Interpreter

The interpreter simply does what's intended according to the parsers results, and contains the code for all the different math operations.

The interpreter could be swapped out for a compiler which generates machine-readable code that your computer can later execute, or could be swapped out for a transpiler which generates code for another language.

## Special Syntax

The interpreter has some uncommon Syntax. Some of them are listed below.

### Numbers

If you don't want to write a Number like so `0.29`, you could write it so `.29` or `42.0` like `42.` but this really makes no sense.

### Square

To square a number use `sqrt(9)` or `sqrt 9` but if don't use parentheses please note that it may lead to "wrong" interpretations. Also you could write `sqrt3(27)` or `sqrt(27,3)` for the third root.

## Code Style & Hacking

Please run the command `black -l 110 *` before do a commit.

To blame someone use `git blame --ignore-revs-file=.git-blame-ignore-revs $FILE` and replace `$FILE` with the file you want to blame.

If you want to contribute to the project feel free todo so.

## API

From now on the API is stable. The current version is 0.1 . If there are versions like 0.1.x the API will not change.

If you think a name of some function in the API not that right, than feel free to open an [issue](https://gitlab.gnome.org/Ungedummt/MathInterpreter/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) to discuss the naming.

## Running the program & test's

Requirements:
 - [Python3](https://www.python.org/downloads/) ^3.8
 - [Black](https://github.com/psf/black#installation-and-usage) (only for code formatting)

### Running test's

To run the test's open shell and enter `./test.sh`.

Alternatively you could run all these commands alone:

 - `$PYTHON3 -m unittest test_lexer`
 - `$PYTHON3 -m unittest test_parser`
 - `$PYTHON3 -m unittest test_interpreter`
 - `$PYTHON3 -m unittest test_main`

### Run the program

Go in the directory above with `cd ..` than run `$PYTHON3 MathInterpreter` where `$PYTHON3` should be replaced with your Python3 binary, often `python` or `python3` will do.
